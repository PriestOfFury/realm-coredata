//
//  ViewController.swift
//  keks)))
//
//  Created by Алёша on 05.05.2020.
//  Copyright © 2020 Алёша. All rights reserved.
//

import UIKit
import RealmSwift

struct HourlyForecast: Decodable {
    var hourly: [Hourly]
    }


struct Weather: Decodable {
    var icon: String
    var description: String
}

struct Hourly: Decodable {
    var dt: Int
    var temp: Float
    var weather: [Weather]
    var feels_like: Float
}

struct DailyForecast: Decodable {
    var daily: [Daily]
}

struct Daily: Decodable {
    var dt: Int
    var temp: Temp
    var weather: [Weather]
}

struct Temp: Decodable {
    var min: Float
    var max: Float
}


var hourlyData = [Hourly]()
var dailyData = [Daily]()



class RealmCurrentData: Object {
    @objc dynamic var cityLabel: String = ""
    @objc dynamic var descriptionLabel: String = ""
    @objc dynamic var tempLabel: String = ""
    @objc dynamic var timeOfParseLabel: String = ""
    @objc dynamic var feelsLikeLabel: String = ""
    @objc dynamic var imageString: String = ""

}


class RealmHourData: Object {
    var dtList = List<Int>()
    var tempList = List<Int>()
    var iconList = List<String>()
}

class RealmDailyData: Object {
    var dateList = List<Int>()
    var maxTempList = List<Int>()
    var minTempList = List<Int>()
    var iconList = List<String>()
    @objc dynamic var tempMin: String = ""
    @objc dynamic var tempMax: String = ""
}

var dtTemporaryArray1 = [Int]()
var tempTemporaryArray1 = [Int]()
var iconTemporaryArray1 = [String]()

var dateTemporaryArray1 = [Int]()
var maxTempTemporaryArray1 = [Int]()
var minTempTemporaryArray1 = [Int]()
var iconTableTemporaryArray1 = [String]()



class ForecastViewController: UIViewController {
    
    
    var realm = try! Realm()
    var realmHourData = RealmHourData()
    var realmDailyData = RealmDailyData()
    
    var dtTemporaryArray2 = [Int]()
    var tempTemporaryArray2 = [Int]()
    var iconTemporaryArray2 = [String]()
    
    var dateTemporaryArray2 = [Int]()
    var maxTempTemporaryArray2 = [Int]()
    var minTempTemporaryArray2 = [Int]()
    var iconTableTemporaryArray2 = [String]()
    
    
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var timeOfParseLabel: UILabel!
    @IBOutlet var feelsLikeLabel: UILabel!
    @IBOutlet var tempMinLabel: UILabel!
    @IBOutlet var tempMaxLabel: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in realm.objects(RealmCurrentData.self) {
            cityLabel.text = i.cityLabel
            descriptionLabel.text = i.descriptionLabel
            tempLabel.text = i.tempLabel
            timeOfParseLabel.text = i.timeOfParseLabel
            feelsLikeLabel.text = i.feelsLikeLabel
            setImageForViewController(from: "https://openweathermap.org/img/wn/" + i.imageString + "@2x.png")
        }
        
        
        
        tableView.layer.borderColor = CGColor(srgbRed: 255, green: 255, blue: 255, alpha: 0.5)
        tableView.layer.borderWidth = 1

        

        loadDailyData()

        loadHourlyData()
        
        
    }
    
    
    func setImageForViewController(from url: String) {
        guard let imageURL = URL(string: url) else { return }
        
        
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.imageView.image = image
            }
        }
    }

    

    
    func loadHourlyData() {
                        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=55&lon=37&exclude=daily&appid=950c2c09eb731277faca1176ecf413e1&units=metric&lang=ru"
                        guard let url = URL(string: urlString) else { return }

                        URLSession.shared.dataTask(with: url) { (data, response, error) in
                            guard let data = data else { return }
                            guard error == nil else { return }

                            do {
                                let json = try JSONDecoder().decode(HourlyForecast.self, from: data)
                               DispatchQueue.main.async {
                                hourlyData = json.hourly
                                let currentRow = json.hourly[0]
                                hourlyData.removeSubrange(25...(hourlyData.count - 1))
                                self.collectionView.reloadData()
                                
                                self.cityLabel.text = "Москва"
                                self.descriptionLabel.text = currentRow.weather[0].description
                                self.tempLabel.text = String(Int(currentRow.temp)) + "°"
                                let currentDate = String(describing: NSDate(timeIntervalSince1970: TimeInterval(currentRow.dt + 10800)))
                                self.timeOfParseLabel.text = "Прогноз актуален на " + currentDate
                                self.feelsLikeLabel.text = "Ощущается \n" + String(Int(currentRow.feels_like)) + "°"
                                let imageString = String(currentRow.weather[0].icon)
                                self.setImageForViewController(from: "https://openweathermap.org/img/wn/" + imageString + "@2x.png")
                                
                                
                                try! self.realm.write {
                                    let newObject = RealmCurrentData()
                                    newObject.cityLabel = self.cityLabel.text!
                                    newObject.descriptionLabel = self.descriptionLabel.text!
                                    newObject.tempLabel = self.tempLabel.text!
                                    newObject.timeOfParseLabel = self.timeOfParseLabel.text!
                                    newObject.feelsLikeLabel = self.feelsLikeLabel.text!
                                    newObject.imageString = currentRow.weather[0].icon
                                    
                                    
                                    self.realm.add(newObject)
                                }

                                
                                //ДОБАВЛЯЮ В РЕАЛМ ДАННЫЕ ДЛЯ КОЛЛЕКШНВЬЮ
                                for object in hourlyData {
                                    dtTemporaryArray1.append(object.dt)
                                    tempTemporaryArray1.append(Int(object.temp))
                                    iconTemporaryArray1.append(object.weather[0].icon)
                                }
                                                                
                                var a = 0
                                while a < dtTemporaryArray1.count {
                                    self.realmHourData.dtList.append(dtTemporaryArray1[a])
                                    self.realmHourData.tempList.append(tempTemporaryArray1[a])
                                    self.realmHourData.iconList.append(iconTemporaryArray1[a])
                                    a += 1
                                }
                                
                                try! self.realm.write {
                                    let newObject = RealmHourData()
                                    newObject.dtList = self.realmHourData.dtList
                                    newObject.tempList = self.realmHourData.tempList
                                    newObject.iconList = self.realmHourData.iconList
                                    
                                    self.realm.add(newObject)
                                }
                                
                                
                                
                                
                                }
                                

        }
                            catch let error { print(error)}
            }
        .resume()
    }
    
    
    func loadDailyData() {
        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=55.75&lon=37.61&exclude=hourly,current&appid=2be7167b716ffef0ae3420dbe7e69feb&units=metric"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            
            do {
                let json = try JSONDecoder().decode(DailyForecast.self, from: data)
                DispatchQueue.main.async {
                    dailyData = json.daily
                    
                    
                    self.tempMinLabel.text = "Мин. \n" + String(Int(dailyData[0].temp.min)) +  "°"
                    self.tempMaxLabel.text = "Макс. \n" + String(Int(dailyData[0].temp.max)) +  "°"
                    
                    
                    
                    for object in dailyData {
                        dateTemporaryArray1.append(object.dt)
                        maxTempTemporaryArray1.append(Int(object.temp.max))
                        minTempTemporaryArray1.append(Int(object.temp.min))
                        iconTableTemporaryArray1.append(object.weather[0].icon)
                    }
                    
                    var a = 0
                    while a < dateTemporaryArray1.count {
                        self.realmDailyData.dateList.append(dateTemporaryArray1[a])
                        self.realmDailyData.maxTempList.append(maxTempTemporaryArray1[a])
                        self.realmDailyData.minTempList.append(minTempTemporaryArray1[a])
                        self.realmDailyData.iconList.append(iconTableTemporaryArray1[a])
                        a += 1
                    }
                    
                    try! self.realm.write {
                        let dailyObject = RealmDailyData()
                        dailyObject.dateList = self.realmDailyData.dateList
                        dailyObject.maxTempList = self.realmDailyData.maxTempList
                        dailyObject.minTempList = self.realmDailyData.minTempList
                        dailyObject.iconList = self.realmDailyData.iconList
                        dailyObject.tempMax = self.tempMaxLabel.text!
                        dailyObject.tempMin = self.tempMinLabel.text!
                        
                        self.realm.add(dailyObject)
                    }
                    
                    self.tableView.reloadData()
                    


                }
            }
            catch let error { print(error)}
        }
    .resume()
        
        self.tableView.reloadData()

    }
    
    

}




extension ForecastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var tableViewCount = 0
        for i in realm.objects(RealmDailyData.self) {
            tableViewCount = i.dateList.count
            //print(count)
            dateTemporaryArray2.append(contentsOf: i.dateList)
            maxTempTemporaryArray2.append(contentsOf: i.maxTempList)
            minTempTemporaryArray2.append(contentsOf: i.minTempList)
            iconTableTemporaryArray2.append(contentsOf: i.iconList)
            tempMaxLabel.text = i.tempMax
            tempMinLabel.text = i.tempMin
        }

        print(tableViewCount)
        return tableViewCount
        

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Table Cell") as! TableViewCell
        let date = String(describing: NSDate(timeIntervalSince1970: TimeInterval(dateTemporaryArray2[indexPath.row]) + 10800))
        var formattedDate = date.dropLast(15)
        formattedDate = formattedDate.dropFirst(8)
        cell.dateLabel.text = String(formattedDate) + " июня"
        
        if cell.dateLabel.text == "31 июня" {
            cell.dateLabel.text = "31 мая"
        }
        
        cell.maxTempLabel.text = String(maxTempTemporaryArray2[indexPath.row]) + "°"

        cell.minTempLabel.text = String(minTempTemporaryArray2[indexPath.row]) + "°"
        cell.setImageForTableView(from: "https://openweathermap.org/img/wn/" + String(iconTableTemporaryArray2[indexPath.row]) + "@2x.png")
        
        
        return cell
    }
}




extension ForecastViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = UIScreen.main.bounds.size.width / 8
        let h = w * 2
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        for i in realm.objects(RealmHourData.self) {
            count = i.dtList.count
            dtTemporaryArray2.append(contentsOf: i.dtList)
            tempTemporaryArray2.append(contentsOf: i.tempList)
            iconTemporaryArray2.append(contentsOf: i.iconList)
            
        }
        return count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Collection Cell", for: indexPath) as! CollectionViewCell
        let date = String(describing: NSDate(timeIntervalSince1970: TimeInterval(dtTemporaryArray2[indexPath.row]) + 10800))
        
        var formattedDate = date.dropLast(12)
        formattedDate = formattedDate.dropFirst(11)
             
        cell.hourLabel.text = String(formattedDate)
        cell.tempLabel.text = String(Int(tempTemporaryArray2[indexPath.row])) + "°"
        cell.setImageForCollectionView(from: "https://openweathermap.org/img/wn/" + iconTemporaryArray2[indexPath.row] + "@2x.png")
        
        return cell
    }
    

}
