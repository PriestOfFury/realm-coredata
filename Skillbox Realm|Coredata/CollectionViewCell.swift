//
//  CollectionViewCell.swift
//  keks)))
//
//  Created by Алёша on 06.05.2020.
//  Copyright © 2020 Алёша. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    

    func setImageForCollectionView(from url: String) {
        guard let imageURL = URL(string: url) else { return }
        
        
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.imageView.image = image
            }
        }
    }
    
    
}

